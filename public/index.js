var socket = io();

var btnTare1    = document.getElementById('btnTare1');
var btnCTare1   = document.getElementById('btnCTare1');
var btnZero1    = document.getElementById('btnZero1');

socket.on('connection', function() {
    console.log('Socket status: Open');
});

socket.on('clientList', function() {
    document.getElementById('clientId').innerHTML = "Client's ID: " + socket.id;
})

socket.on('WhpeltuINDvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue1whPELTU');
    scale.innerHTML = `${data}`;
});

socket.on('WhpeltuINDuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM1whPELTU');
    scale.innerHTML = `${data}`;
});

socket.on('WhpeltuICSvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue2whPELTU');
    scale.innerHTML = `${data}`;
});

socket.on('WhpeltuICSuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM2whPELTU');
    scale.innerHTML = `${data}`;
});

socket.on('WhppXPvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue3whPP');
    scale.innerHTML = `${data}`;
});

socket.on('WhppXPuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM3whPP');
    scale.innerHTML = `${data}`;
});

socket.on('WhsmdXPvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue4whSMD');
    scale.innerHTML = `${data}`;
});

socket.on('WhsmdXPuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM4whSMD');
    scale.innerHTML = `${data}`;
});

socket.on('PmdINDvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue5PMD');
    scale.innerHTML = `${data}`;
});

socket.on('PmdINDuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM5PMD');
    scale.innerHTML = `${data}`;
});

socket.on('KitchenXPvalue', function(data) {
    // console.log(data);
    let scale = document.getElementById('scalingValue6KITCHEN');
    scale.innerHTML = `${data}`;
});

socket.on('KitchenXPuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM6KITCHEN');
    scale.innerHTML = `${data}`;
});

socket.on('KretekINDvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue7KRETEK');
    scale.innerHTML = `${data}`;
});

socket.on('KretekINDuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM7KRETEK');
    scale.innerHTML = `${data}`;
});

socket.on('KretekICSvalue', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingValue8KRETEK');
    scale.innerHTML = `${data}`;
});

socket.on('KretekICSuom', function(data) {
    //console.log(data);
    let scale = document.getElementById('scalingUOM8KRETEK');
    scale.innerHTML = `${data}`;
});

$('#btnTare1').click(function(data) {
    console.log('Tare');
    socket.emit('tare', function() {
        
    });
});

$('#btnCTare1').click(function(data) {
    console.log('CTare');
    socket.emit('ctare', function() {
        
    });
});

$('#btnZero1').click(function(data) {
    console.log('Zero');
    socket.emit('zero', function() {
        
    });
});