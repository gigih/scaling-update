// KRETEK - IND246 ICS425

var http = require('http');
var express = require('express');
var socketIO = require('socket.io');

var app = express();
var server = http.createServer(app);
var io = socketIO.listen(server);

var SerialPort = require('serialport');
var Readline = SerialPort.parsers.Readline;
var MyPort1;
var MyPort2;
var MyPort = [];

app.set('port', '3000');
app.use(express.static(__dirname + '/public'));
app.get('/', function (req, res, next) {
    res.sendFile(__dirname + '/public/index.html');
});

server.listen(3000, function () {
    console.log('Server port:', 3000);
    console.log('Server status: Open');
});

io.on('connection', function (socket) {
    console.log('Client status: Connected');

    socket.on('disconnect', function () {
        console.log('Client status: Disconnected');
    });

    io.clients(function (error, clients) {
        if (error) throw error;
        io.emit('clientList', clients);
        console.log(clients);
    });

    socket.on('tare', function(data) {
        var buf1 = Buffer.from(['0x1b']);
        var buf2 = Buffer.from('T_\r\n');
        var buf = Buffer.concat([buf1, buf2], buf1.length + buf2.length);
        console.log(buf);
        //sp1.write(buf);
        
        console.log(data);
        console.log('Client : Tare');
    });

    socket.on('ctare', function() {
        //port.write(data);
        console.log('Client : CTare');
    });

    socket.on('zero', function() {
        //port.write(data);
        console.log('Client : Zero');
    });
});

var getConnected = function () {
    SerialPort.list(function (err, ports) {
        var allports = ports.length;
        var count = 0;
        var done = false

        ports.forEach(function (port) {
            count += 1;
            pm = port['serialNumber'];
            if (typeof pm !== 'undefined' && pm.includes('&0&1')) {
            //if (typeof pm !== 'undefined' && pm.includes('FTDI')) {
                MyPort[0] = port.comName.toString();
                //SerialPort = require('serialport');

                var sp1 = new SerialPort(MyPort[0], {
                    buadRate: 9600,
                    dataBits: 8,
                    parity: 'none',
                    stopBits: 1
                });

                var parser1 = sp1.pipe(new Readline({ delimeter: '\r\n' }));

                sp1.on('open', function () {
                    console.log('Serial status: Open');
                    console.log('Serial status: Connected at ' + MyPort[0]);
                });

                parser1.on('data', function (data) {
                    var str = new String(data);
                    var values = str.split(' ');

                    console.log(data);
                    //console.log(values[2]);
                    
                    io.emit('KretekINDvalue', values[7]);
                    io.emit('KretekINDuom', values[8]);
                });

                sp1.on('close', function () {
                    console.log('Serial status: Close');
                    getReconnect();
                });

                sp1.on('error', function (err) {
                    console.log(err);
                    getReconnect();
                });

                done = true;
            }

            if (typeof pm !== 'undefined' && pm.includes('&0&2')) {
                //if (typeof pm !== 'undefined' && pm.includes('FTDI')) {
                MyPort[1] = port.comName.toString();
                //serialPort = require('serialport');

                var sp2 = new SerialPort(MyPort[1], {
                    buadRate: 9600,
                    dataBits: 8,
                    parity: 'none',
                    stopBits: 1
                });

                var parser2 = sp2.pipe(new Readline({ delimeter: '\r\n' }));

                sp2.on('open', function () {
                    console.log('Serial status: Open');
                    console.log('Serial status: Connected at ' + MyPort[1]);
                });

                parser2.on('data', function (data) {
                    //let scale = parseFloat(data);
                    //console.log(scale);
                    var str = new String(data);
                    var values = str.split(' ');

                    //console.log('Data 1: ' + data);
                    //console.log(data);
                    //console.log(values[2]);
                    //io.emit('data1', data);
                    io.emit('KretekICSvalue', values[7]);
                    io.emit('KretekICSuom', values[8]);
                });

                sp2.on('close', function () {
                    console.log('Serial status: Close');
                    getReconnect();
                });

                sp2.on('error', function (err) {
                    console.log(err);
                    getReconnect();
                });

                done = true;
            }

            if (count === allports && done === false) {
                console.log('Serial status: Not Found');
                getReconnect();
            }
        });
    });
}

getConnected();

var getReconnect = function () {
    setTimeout(function () {
        console.log('Serial status: Reconnect');
        getConnected();
    }, 2000);
};